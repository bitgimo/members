<!DOCTYPE html>
<html>
<head>
    <title>PHP Jquery Ajax CRUD Example - pakainfo.com</title>
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>
 
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.3.1/jquery.twbsPagination.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="js/member-ajax.js"></script>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">                    
                <div class="pull-left">
                    <h2>PHP Jquery Ajax CRUD Example</h2>
					<b>PHP Jquery Ajax CRUD Example Tutorial From Scratch</b>
                </div>
                <div class="pull-right">
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#create-member">
                      Create Member
                </button>
                </div>
            </div>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                <th>Member Name</th>
                <th>Member Information</th>
                <th width="200px">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <ul id="pagination" class="pakainfo pagination-sm"></ul>
        <!-- amke Member Modal -->
        <div class="modal fade" id="create-member" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Create Member</h4>
              </div>
              <div class="modal-body">
                      <form data-toggle="validator" action="api/create.php" method="POST">
                          <div class="form-group">
                            <label class="control-label" for="membername">Member Name:</label>
                            <input type="text" name="membername" class="form-control" data-error="Please enter membername." required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="membername">Member Information:</label>
                            <textarea name="member_info" class="form-control" data-error="Please enter member_info." required></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn crud-submit btn-success">Submit</button>
                        </div>
                      </form>
              </div>
            </div>
          </div>
        </div>
        <!-- Edit Member Modal -->
        <div class="modal fade" id="edit-member" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Member</h4>
              </div>
              <div class="modal-body">
                      <form data-toggle="validator" action="api/update.php" method="put">
                          <input type="hidden" name="id" class="edit-id">
                          <div class="form-group">
                            <label class="control-label" for="membername">Member Name:</label>
                            <input type="text" name="membername" class="form-control" data-error="Please enter membername." required />
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label" for="membername">Member Information:</label>
                            <textarea name="member_info" class="form-control" data-error="Please enter member_info." required></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success crud-submit-edit">Submit</button>
                        </div>
                      </form>
              </div>
            </div>
          </div>
        </div>
    </div>
</body>
</html>