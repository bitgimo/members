$( document ).ready(function() {
    var page = 1;
    var current_page = 1;
    var total_page = 0;
    var is_ajax_fire = 0;
    manageData();
    /* manage data list */
    function manageData() {
        $.ajax({
            dataType: 'json',
            url: 'api/fetchMemberData.php',
            data: {page:page}
        }).done(function(data){
            total_page = Math.ceil(data.total/10);
            current_page = page;
            $('#pagination').twbsPagination({
                totalPages: total_page,
                visiblePages: current_page,
                onPageClick: function (event, pageL) {
                    page = pageL;
                    if(is_ajax_fire != 0){
                      getPageData();
                    }
                }
            });
            manageRow(data.data);
            is_ajax_fire = 1;
        });
    }
    /* Get Page Data*/
    function getPageData() {
        $.ajax({
            dataType: 'json',
            url: 'api/fetchMemberData.php',
            data: {page:page}
        }).done(function(data){
            manageRow(data.data);
        });
    }
    /* Add new Member table row */
    function manageRow(data) {
        var    rows = '';
        $.each( data, function( key, value ) {
              rows = rows + '<tr>';
              rows = rows + '<td>'+value.membername+'</td>';
              rows = rows + '<td>'+value.member_info+'</td>';
              rows = rows + '<td data-id="'+value.id+'">';
            rows = rows + '<button data-toggle="modal" data-target="#edit-member" class="btn btn-primary edit-member">Edit</button> ';
            rows = rows + '<button class="btn btn-danger remove-member">Delete</button>';
            rows = rows + '</td>';
              rows = rows + '</tr>';
        });
        $("tbody").html(rows);
    }
    /* Create new Member */
    $(".crud-submit").click(function(e){
        e.preventDefault();
        var form_action = $("#create-member").find("form").attr("action");
        var membername = $("#create-member").find("input[name='membername']").val();
     
        var member_info = $("#create-member").find("textarea[name='member_info']").val();
        if(membername != '' && member_info != ''){
            $.ajax({
                dataType: 'json',
                type:'POST',
                url: form_action,
                data:{membername:membername, member_info:member_info}
            }).done(function(data){
                $("#create-member").find("input[name='membername']").val('');
                $("#create-member").find("textarea[name='member_info']").val('');
                getPageData();
                $(".modal").modal('hide');
                toastr.success('Member Created Successfully.', 'Success Alert', {timeOut: 5000});
            });
        }else{
            alert('You are missing membername or member_info.')
        }
    });
    /* Remove Member */
    $("body").on("click",".remove-member",function(){
        var id = $(this).parent("td").data('id');
        var c_obj = $(this).parents("tr");
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: 'api/delete.php',
            data:{id:id}
        }).done(function(data){
            c_obj.remove();
            toastr.success('Member Deleted Successfully.', 'Success Alert', {timeOut: 5000});
            getPageData();
        });
    });
    /* Edit Member */
    $("body").on("click",".edit-member",function(){
        var id = $(this).parent("td").data('id');
        var membername = $(this).parent("td").prev("td").prev("td").text();
        var member_info = $(this).parent("td").prev("td").text();
     
        $("#edit-member").find("input[name='membername']").val(membername);
        $("#edit-member").find("textarea[name='member_info']").val(member_info);
        $("#edit-member").find(".edit-id").val(id);
    });
    /* Updated new Member */
    $(".crud-submit-edit").click(function(e){
        e.preventDefault();
        var form_action = $("#edit-member").find("form").attr("action");
        var membername = $("#edit-member").find("input[name='membername']").val();
        var member_info = $("#edit-member").find("textarea[name='member_info']").val();
        var id = $("#edit-member").find(".edit-id").val();
        if(membername != '' && member_info != ''){
            $.ajax({
                dataType: 'json',
                type:'POST',
                url: form_action,
                data:{membername:membername, member_info:member_info,id:id}
            }).done(function(data){
                getPageData();
                $(".modal").modal('hide');
                toastr.success('Member Updated Successfully.', 'Success Alert', {timeOut: 5000});
            });
        }else{
            alert('You are some missing membername or member_info.')
        }
    });
    });